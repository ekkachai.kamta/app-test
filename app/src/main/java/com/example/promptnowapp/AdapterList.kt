package com.example.promptnowapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_item.view.*

class AdapterList(
private val recycleViewCallback: RecycleViewCallback
) : RecyclerView.Adapter<AdapterList.ViewHV>() {

    var list = mutableListOf<ItemTest>()

    class ViewHV(itemView: View): RecyclerView.ViewHolder(itemView){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHV {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_item,parent,false)
        return ViewHV(itemView)
    }

    override fun onBindViewHolder(holder: ViewHV, position: Int) {
            var data = list[position]
            holder.itemView.apply {
                name.text = list[position].name
                checkbox.setOnClickListener {
                    Log.i("dfd",checkbox.isChecked.toString())
                    data.check = checkbox.isChecked
                    recycleViewCallback.itemClick(data,position)

                }

            }

    }

    override fun getItemCount(): Int = list.size
}

interface RecycleViewCallback{
    fun itemClick(item: ItemTest, i: Int)
}