package com.example.promptnowapp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),RecycleViewCallback {

    private lateinit var adapterList: AdapterList
    private var data = mutableListOf<ItemTest>()

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapterList = AdapterList(this)


        list?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter  = adapterList
        }


        input.addTextChangedListener {
            if(it.isNullOrEmpty()){
                add.setBackgroundColor(ContextCompat.getColor(this, R.color.hex_color))
                add.isEnabled = false
            }else{
                add.setBackgroundColor(ContextCompat.getColor(this, R.color.active_color))
                add.isEnabled = true
            }
        }

        add.setOnClickListener {
            data.add(ItemTest(input.text.toString()))
           setData()

        }

        delete.setOnClickListener {
            var list = data
            list.forEachIndexed { index, itemTest ->
                if(itemTest.check){
                    data.removeAt(index)
                }
            }
            setData()

        }

    }


    private fun setData(){
        adapterList.apply {
            list = data
            notifyDataSetChanged()

        }
    }

    override fun itemClick(item: ItemTest, i: Int) {
            data.forEachIndexed { index, itemTest ->
                if(index == i){
                    itemTest.check = item.check
                }
            }
    }
}